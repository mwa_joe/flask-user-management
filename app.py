#!/usr/bin/env python3

# need this
from models import app, User, db

# and a couple of modules from flask
from flask import request, render_template, flash, redirect, url_for, session, abort, g

# using flask_login. create and instance of LoginManager
from flask_login import LoginManager, login_user, logout_user, current_user, login_required

# initialize with app instance
login_manager = LoginManager()
login_manager.init_app(app)

# redirect users to login view wherenever they need to be logged in
login_manager.login_view = 'login'

# load the user from an id
@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

############### Views

@app.route('/')
def index():
    return render_template('index.html')

# register
@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    user = User(request.form['username'], request.form['password'], request.form['email'])
    db.session.add(user)
    db.session.commit()
    flash('User successfully registered')
    return redirect(url_for('login'))

# login
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    username = request.form['username']
    password = request.form['password']
    remember_me = False

    if 'remember_me' in request.form:
        remember_me = True

    registered_user = User.query.filter_by(username=username, password=password).first()
    if registered_user is None:
        flash('Username or Password is invalid')
        return redirect(url_for('login'))
    login_user(registered_user)
    flash('logged in successfully')
    return redirect(request.args.get('next') or url_for('index'))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.route('/home')
@login_required
def home():
    return render_template('home.html')
#
# remove this part when using an application server eg gunicorn
if __name__ == '__main__':
    app.secret_key= 'this is a secret'
    app.run()
