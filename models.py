#!/usr/bin/env python3

import builtins

# of course this is needed
from flask import Flask

# get db orm and driver
import psycopg2
from flask_sqlalchemy import SQLAlchemy

# migrate for alembic
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

# initialize app
app = Flask(__name__)

# development mode, be able to see bugs
app.config['DEBUG'] = True

# connect to db
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:dbuser@192.168.0.102:5432/auth'

# if i don't do this i get errors in terminal
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# db access point
db = SQLAlchemy(app)

# to make it migratable
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


# describe db model class example from https://blog.openshift.com/use-flask-login-to-add-user-authentication-to-your-python-application/ here
class User(db.Model):
    __tablename__ = 'users'
    id           = db.Column('user_id', db.Integer, primary_key=True, autoincrement=True)
    username     = db.Column('username', db.VARCHAR(100), unique=True, index=True)
    password     = db.Column('password', db.VARCHAR(100))
    email        = db.Column('email', db.VARCHAR(100), unique=True)
    registered_on= db.Column('registered_on', db.DateTime)

    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    # need to implement 4 methods that work with Flask-login
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return int(self.id)

    def __repr__(self):
        return "<User {}>".format(self.username)

# run it
if __name__ == '__main__':
    manager.run()
